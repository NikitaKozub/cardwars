using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hand : MonoBehaviour
{
    public List<Card> CardInHand;
    public event UnityAction GetCard;

    private Terrain[] _terrain;
    // Start is called before the first frame update
    public void StartHand(Terrain[] terrain)
    {
        _terrain = terrain;
        CardInHand = new List<Card>(10);
    }

    public void AddCardInHand(Card card)
    {
        GetCard?.Invoke();
        if (CardInHand.Count < 10) {
            CardInHand.Add(card);
            card.MoveCardInTerrain += GetCardInTerrain;
            card.CardMouseUp += GetTerrain; // -=
            RecalculatePositionCardInHand();
        }
    }
    
    public void RemoveCardInHand(Card card)
    {
        card.MoveCardInTerrain -= GetCardInTerrain;
    }

    public void RecalculatePositionCardInHand()
    {
        //float newRotation = -25.0f;
        //float Step = 50 / CardInHand.Count;
        for (int i = 0; i < CardInHand.Count; i++)
        {
            CardInHand[i].transform.position = new Vector3((5*i + this.transform.position.x)/ CardInHand.Count, this.transform.position.y, this.transform.position.z);
            CardInHand[i].PositionHandX = CardInHand[i].transform.position.x;
            CardInHand[i].PositionHandY = CardInHand[i].transform.position.y;
            CardInHand[i].PositionHandZ = CardInHand[i].transform.position.z;

            //newRotation = newRotation + Step;
            //CardInHand[i].transform.Rotate(new Vector3(0, 0, newRotation));
        }
    }

    public void GetCardInTerrain(Card card)
    {
        for (int i = 0; i < _terrain.Length; i++)
        {
            if (Mathf.Abs(_terrain[i].transform.position.x - card.transform.position.x) < 1)
            {
                StartCoroutine(_terrain[i].Blink());
                card.Terrain = _terrain[i];
                card.IsOwnerTerrain = true;
                break;
            }
        }
    }

    public void GetTerrain(Card card)
    {
        card.Terrain.CardOnTerrain = card;
    }
}
