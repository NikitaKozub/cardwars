using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ManagerFight : MonoBehaviour
{
    [SerializeField] private Button EndTurn;

    public event UnityAction<int> StartTurnGame;
    public event UnityAction EndTurnGame;

    private void Start()
    {
        StartTurnGame?.Invoke(1);
    }
    private void OnEnable()
    {
        EndTurn.onClick.AddListener(_finishTurn);
    }
    private void OnDisable()
    {
        EndTurn.onClick.RemoveListener(_finishTurn);
    }
    private void _finishTurn()
    {
        EndTurnGame?.Invoke();
    }
}
