using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Cards;
using System.IO;
using System.Linq;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public int Hp = 25;

    public string NameCurrentDeck;
    public List<Deck> Decks;
    public List<Card> CurrentDeck;
    public CardData[] CurrentDataDeck;
    public Card[] DiscardDeck;
    private string FILENAME;
    private string filePath;

    private int _countDecks;
    public Field currentFieldForCreateDeck;
    [SerializeField] public Terrain[] TerrainPlayer;
    public Hand HandPlayer;
    [SerializeField] private GameObject _positionDeck;
    [SerializeField] private GameObject _positionHand;
    void OnEnable()
    {
        HandPlayer.StartHand(TerrainPlayer);
        currentFieldForCreateDeck = Field.blue_plains;
        CurrentDataDeck = new CardData[15];
        CurrentDeck = new List<Card>(15);
        Decks = new List<Deck>();
        filePath = Path.Combine(Application.dataPath + "/Decks/");
        _countDecks = Directory.GetFiles(filePath, "*.json").Length;
        files = Directory.GetFiles(filePath, "*.json");
    }
    string[] files;
    public Deck[] GetDecks()
    {
        Deck[] deck = new Deck[_countDecks];
        if (_countDecks > 0)
        {
            for (int i = 0; i < _countDecks; i++)
            {
                deck[i] = new Deck(StorageCard.LoadDataFromFile(files[i]));
                deck[i].Name = files[i];
            }
        }
        return deck;
    }

    public void CreateCardForGame(List<Card[]> listCards)
    {
        foreach(var fieldCard in listCards)
        {
            for(int i = 0; i < CurrentDataDeck.Length; i++)
            {
                for (int j = 0; j < fieldCard.Length; j++)
                {
                    if (CurrentDataDeck[i].Number == fieldCard[j].Date.Number)
                    {
                        CurrentDeck.Add(Instantiate(fieldCard[j], _positionDeck.transform));
                        CurrentDeck.Last().Render();
                    }
                }
            }
        }
    }

    public void ReceiveDamage(int damage)
    {
        Hp -= damage;
        TakeDamage?.Invoke();
    }
    public event UnityAction TakeDamage;
}
