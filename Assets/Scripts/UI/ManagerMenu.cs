using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerMenu : MonoBehaviour
{
    [SerializeField] private Button _shop;
    [SerializeField] private Button _createDeck;
    [SerializeField] private Button _managerDeck;
    [SerializeField] private Button _exit;

    private GameObject _shopPanel;
    private bool _isShopPanel = false;
    private GameObject _createDeckPanel;
    private bool _isCreateDeckPanel = false;
    private GameObject _managerDeckPanel;
    private bool _isManagerDeckPanel = false;

    private void Awake()
    {
        _shopPanel = GameObject.Find("PanelShopCards");
        _shopPanel.SetActive(_isShopPanel);
        _createDeckPanel = GameObject.Find("PanelCreateDeck");
        _createDeckPanel.SetActive(_isCreateDeckPanel);
        _managerDeckPanel = GameObject.Find("PanelManagerDecks");
        _managerDeckPanel.SetActive(_isManagerDeckPanel);
    }
    private void OnEnable()
    {
        _shop.onClick.AddListener(_activeShop);
        _createDeck.onClick.AddListener(_activeCreateDeck);
        _managerDeck.onClick.AddListener(_activeManagerDeck);
        _exit.onClick.AddListener(_closeApplication);
    }
    public void OnDisable()
    {
        _shop.onClick.RemoveListener(_activeShop);
        _createDeck.onClick.RemoveListener(_activeCreateDeck);
        _managerDeck.onClick.RemoveListener(_activeManagerDeck);
        _exit.onClick.RemoveListener(_closeApplication);
    }
    public void _activeShop()
    {
        _isShopPanel = !_isShopPanel;
        _shopPanel.SetActive(_isShopPanel);

        _isCreateDeckPanel = false;
        _createDeckPanel.SetActive(_isCreateDeckPanel);
        _isManagerDeckPanel = false;
        _managerDeckPanel.SetActive(_isManagerDeckPanel);
    }
    public void _activeCreateDeck()
    {
        _isCreateDeckPanel = !_isCreateDeckPanel;
        _createDeckPanel.SetActive(_isCreateDeckPanel);

        _isShopPanel = false;
        _shopPanel.SetActive(_isShopPanel);
        _isManagerDeckPanel = false;
        _managerDeckPanel.SetActive(_isManagerDeckPanel);
    }
    public void _activeManagerDeck()
    {
        _isManagerDeckPanel = !_isManagerDeckPanel;
        _managerDeckPanel.SetActive(_isManagerDeckPanel);

        _isShopPanel = false;
        _shopPanel.SetActive(_isShopPanel);
        _isCreateDeckPanel = false;
        _createDeckPanel.SetActive(_isCreateDeckPanel);
    }
    public void _closeApplication()
    {
        Application.Quit();
    }
}