using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ButtonFieldSwitch : MonoBehaviour
{
    public Field ButtonField;
    public Image ImageField;
    public Button Switch;
    public event UnityAction<Field/*, ButtonFieldSwitch*/> SwitchField;

    private void OnEnable()
    {
        Switch.onClick.AddListener(SwitchFieldForViewCard);
    }

    private void OnDisable()
    {
        Switch.onClick.RemoveListener(SwitchFieldForViewCard);
    }

    public void Render(Field field, Sprite image)
    {
        ButtonField = field;
        ImageField.sprite = image;
    }

    private void SwitchFieldForViewCard()
    {
        SwitchField?.Invoke(ButtonField/*, this*/);
    }
}
