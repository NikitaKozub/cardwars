using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.IO;
using UnityEditor;

public class CardView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Text _countCard;
    [SerializeField] private Button _getCard;

    private Field _field;
    private string _path;
    public event UnityAction<string, Field> BuyCard;
    private Card _card;
    private void OnEnable()
    {
        _getCard.onClick.AddListener(GetCardDeck);
    }

    private void OnDisable()
    {
        _getCard.onClick.RemoveListener(GetCardDeck);
    }
    public void Render(Card card, Field field)
    {
        _card = card;
        Sprite image = Resources.Load<Sprite>(Path.Combine("Image/Card/" + _card.Date.Number));
        _countCard.text = _card.Date.CountPlayer.ToString();
        _image.sprite = image;

        _field = field;
        _path = Path.Combine(Application.dataPath + "/" + "CardsCollection" + field + ".json");
    }

    public void GetCardDeck()
    {
        _card.Date.CountPlayer++;
        _countCard.text = _card.Date.CountPlayer.ToString();
        BuyCard?.Invoke(_path, _field);
    }
}
