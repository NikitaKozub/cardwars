using Assets.Scripts;
using Assets.Scripts.Cards;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Card[] BluePlains;
    /// <summary>
    /// ������ ������ ��� ������������ ����� ������� �� ����
    /// </summary>
    [SerializeField] private ButtonFieldSwitch _templateFields;
    /// <summary>
    /// ����� ��� ����� ������ ��� ������������ ����� ������� �� �� ����
    /// </summary>
    [SerializeField] private GameObject _contentfieldsView;
    /// <summary>
    /// ������ �����
    /// </summary>
    [SerializeField] private CardView _templateCard;
    /// <summary>
    /// ����� ��� ����� ���������� �����
    /// </summary>
    [SerializeField] private GameObject _contentCardView;

    Player player; 
    string FILENAME; 
    string filePath;
    void Awake()
    {
        DirectoryInfo dir = new DirectoryInfo(Path.Combine(Application.dataPath + "/Resources/Prefabs/PrefabsCards/"));
        countFields = dir.GetDirectories().Length;

        player = GameObject.Find("Player").GetComponent<Player>();
    }
    int countFields;
    ButtonFieldSwitch[] buttonFieldSwitch;
    Field currentField;
    private void OnEnable()
    {
        Sprite[] FieldsIcons = Resources.LoadAll<Sprite>(Path.Combine("Prefabs/UI/FiledsIcons/"));
        buttonFieldSwitch = new ButtonFieldSwitch[FieldsIcons.Length];
        foreach (Field field in Enum.GetValues(typeof(Field)))
        {
            buttonFieldSwitch[(int)field] = Instantiate(_templateFields, _contentfieldsView.transform);
            _templateFields.Render(field, FieldsIcons[(int)field]);
            buttonFieldSwitch[(int)field].SwitchField += _render;
        }
    }

    private void OnDisable()
    {
        foreach(var itr in buttonFieldSwitch)
        {
            itr.SwitchField -= _render;
        }
        if (_cardViews != null)
        {
            for (int i = 0; i < _cardViews.Length; i++)
            {
                _cardViews[i].BuyCard -= StorageCard.SaveToFileCards;
            }
        }

        foreach (Transform child in _contentfieldsView.transform)
        {
            Destroy(child.gameObject);// �������
        }
    }
    Card[] Cards;
    CardData[] cardDatas;
    private CardView[] _cardViews;
    private void _render(Field field/*, ButtonFieldSwitch buttonFieldSwitch*/)
    {
        currentField = field; 
        FILENAME = "CardsCollection" + currentField + ".json";
        filePath = Path.Combine(Application.dataPath + "/" + FILENAME);

        Cards = Resources.LoadAll<Card>(Path.Combine("Prefabs/PrefabsCards/" + (int)currentField));
        _cardViews = new CardView[Cards.Length];
        if (File.Exists(filePath))
        {
            cardDatas = StorageCard.LoadDataFromFile(currentField);
        }
        else
        {
            StorageCard.SaveToFileCards(filePath, currentField);
        }
        
        foreach (Transform child in _contentCardView.transform)
        {
            Destroy(child.gameObject);// �������
        }
        
        for (int i = 0; i < cardDatas.Length; i++)
        {
            CardView cardView = Instantiate(_templateCard, _contentCardView.transform);
            _cardViews[i] = cardView;
            for (int j = 0; j < Cards.Length; j++)
            {
                if (cardDatas[i].Number == Cards[j].Date.Number)
                {
                    Cards[j].Date.CountPlayer = cardDatas[i].CountPlayer;
                    cardView.Render(Cards[j], currentField);
                    cardView.BuyCard += StorageCard.SaveToFileCards;
                    break;
                }
            }
        }
    }
}