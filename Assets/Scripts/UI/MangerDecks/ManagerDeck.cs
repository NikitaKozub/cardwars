using Assets.Scripts.Cards;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ManagerDeck : MonoBehaviour
{
    Deck[] decks;
    private Player _player;

    [SerializeField] private Button _buttonCreateDeck;
    [SerializeField] private GameObject _positionButtonCreateDeck;

    ButtonFieldSwitch[] buttonFieldSwitch;
    [SerializeField] private ButtonFieldSwitch _buttonSwitchField;
    [SerializeField] private GameObject _positionButtonSwitchField;

    [SerializeField] private DeckView _buttonSelectDeck;

    [SerializeField] private GameObject _createDeck;

    private void Awake()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _createDeck = GameObject.Find("PanelCreateDeck");
    }
    private void OnEnable()
    {
        Sprite[] FieldsIcons = Resources.LoadAll<Sprite>(Path.Combine("Prefabs/UI/FiledsIcons/"));
        buttonFieldSwitch = new ButtonFieldSwitch[FieldsIcons.Length];
        foreach (Field field in Enum.GetValues(typeof(Field)))
        {
            buttonFieldSwitch[(int)field] = Instantiate(_buttonSwitchField, _positionButtonSwitchField.transform);
            _buttonSwitchField.Render(field, FieldsIcons[(int)field]);
            buttonFieldSwitch[(int)field].SwitchField += _createField;
        }
    }

    private void OnDisable()
    {
        foreach (var itr in buttonFieldSwitch)
        {
            itr.SwitchField -= _createField;
        }
        foreach (Transform child in _positionButtonSwitchField.transform)
        {
            Destroy(child.gameObject);// �������
        }

        _buttonCreateDeck.onClick.AddListener(_openCreateDeck);
    }

    private void _createField(Field field)
    {
        _player.currentFieldForCreateDeck = field;
    }
    // Start is called before the first frame update
    void Start()
    {
        decks = _player.GetDecks();
        DeckView[] deckView = new DeckView[decks.Length];
        for(int i = 0; i < deckView.Length; i++)
        {
            deckView[i] = Instantiate(_buttonSelectDeck, _positionButtonCreateDeck.transform);
            deckView[i].Render(decks[i]);
        }
        _buttonCreateDeck = (Button)GameObject.Instantiate(_buttonCreateDeck, _positionButtonCreateDeck.transform);
        _buttonCreateDeck.onClick.AddListener(_openCreateDeck);
    }

    public void _openCreateDeck()
    {
        _createDeck.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
