using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Text _name;
    [SerializeField] private Button _selectDeck;
    public Deck CurrentDeck;
    private Player _player;

    public void Render(Deck deck)
    {
        CurrentDeck = deck;

        _name.text = CurrentDeck.Name;
    }

    private void OnEnable()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();

        _selectDeck.onClick.AddListener(SwitchFieldForViewCard);
    }

    private void OnDisable()
    {
        _selectDeck.onClick.RemoveListener(SwitchFieldForViewCard);
    }

    private void SwitchFieldForViewCard()
    {
        StorageCard.SaveToFileCurrentDeck(CurrentDeck.Cards);
    }
}
