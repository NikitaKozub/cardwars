using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInDeckView : MonoBehaviour
{
    [SerializeField] private Text _name;
    [SerializeField] private Text _cost;
    [SerializeField] private Image _field;
    public int Number;

    public void Render(CardData cardData)
    {
        _name.text = cardData.Name;
        _cost.text = cardData.Cost.ToString();
        _field.sprite = StorageCard.FieldsIcons[(int)cardData.Field];
        Number = cardData.Number;

        name = cardData.Name;
    }

    public void Delete()
    {
        Destroy(this.gameObject);
    }
}
