using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateDeckPlayer : MonoBehaviour
{
    /// <summary>
    /// ������ ���� � ���������
    /// </summary>
    [SerializeField] private CreateDeckView _templateCardView;
    /// <summary>
    /// ������� ���� � ���������
    /// </summary>
    [SerializeField] private GameObject _positionTemplateCardView;
    /// <summary>
    /// ������ ���� ������� ����� � ����
    /// </summary>
    [SerializeField] private CardInDeckView _templateCardInDeck;
    /// <summary>
    /// ������� ���� ������� ����� � ����
    /// </summary>
    [SerializeField] private GameObject _positionTemplateCardInDeck;
    /// <summary>
    /// ������� ���� ������� ����� � ����
    /// </summary>
    [SerializeField] private Button _buttonSaveDeck;

    private Player _player;
    CreateDeckView[] createDeckView;
    private void OnEnable()
    {
        foreach (Transform child in _positionTemplateCardView.transform)
        {
            Destroy(child.gameObject);// �������
        }
        foreach (Transform child in _positionTemplateCardInDeck.transform)
        {
            Destroy(child.gameObject);// �������
        }
        _buttonSaveDeck.onClick.AddListener(_saveDeck);

        newDeck = new List<CardData>(30);
        cardInDeck = new List<CardInDeckView>(30);
        _player = GameObject.Find("Player").GetComponent<Player>();
        CardData[] cardDatas = StorageCard.LoadDataFromFile(_player.currentFieldForCreateDeck);
        createDeckView = new CreateDeckView[cardDatas.Length];
        for (int i = 0; i < cardDatas.Length; i++)
        {
            createDeckView[i] = Instantiate(_templateCardView, _positionTemplateCardView.transform);
            createDeckView[i].Render(cardDatas[i]);
            createDeckView[i].AddCard += _addCardInDeck;
            createDeckView[i].RemoveCard += _removeCardInDeck;
        }
    }

    public List<CardData> newDeck;
    public List<CardInDeckView> cardInDeck;
    private void _addCardInDeck(CardData cardData)
    {
        newDeck.Add(cardData);

        CardInDeckView card = Instantiate(_templateCardInDeck, _positionTemplateCardInDeck.transform);
        cardInDeck.Add(card);
        card.Render(cardData);
    }
    private void _removeCardInDeck(CardData cardData)
    {
        newDeck.Remove(cardData);
        CardInDeckView card = cardInDeck.FindLast(c => c.Number == cardData.Number);
        card.Delete();
        cardInDeck.Remove(card);
    }

    private void OnDisable()
    {
        foreach (var itr in createDeckView)
        {
            itr.AddCard -= _addCardInDeck;
        }
        foreach (var itr in createDeckView)
        {
            itr.RemoveCard -= _removeCardInDeck;
        }
        _buttonSaveDeck.onClick.RemoveListener(_saveDeck);
    }

    private void _saveDeck()
    {
        if (newDeck.Count == 15)
        {
            if(StorageCard.SaveToFileDeck("Deck-", newDeck.ToArray()))
            {
                _player.CurrentDataDeck = newDeck.ToArray();
            }
        }
    }
}
