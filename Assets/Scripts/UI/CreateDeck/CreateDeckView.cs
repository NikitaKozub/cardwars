using Assets.Scripts.Cards;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CreateDeckView : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Text _countCard;
    [SerializeField] private Text _countCardInDeck;
    [SerializeField] private Button _getCard;
    [SerializeField] private Button _giveCard;

    private int currentCountThisCardInDeck;
    private Field _field;
    private string _path;
    public event UnityAction<CardData> AddCard;
    public event UnityAction<CardData> RemoveCard;
    private CardData _cardData;
    private void OnEnable()
    {
        _getCard.onClick.AddListener(GetCardInDeck);
        _giveCard.onClick.AddListener(GiveCardInDeck);
    }
    private void OnDisable()
    {
        _getCard.onClick.RemoveListener(GetCardInDeck);
        _giveCard.onClick.RemoveListener(GiveCardInDeck);
    }
    public void Render(CardData cardData)
    {
        _cardData = cardData;
        Sprite image = Resources.Load<Sprite>(Path.Combine("Image/Card/" + _cardData.Number));
        _image.sprite = image;
        _path = Path.Combine(Application.dataPath + "/" + "CardsCollection" + _cardData.Field + ".json");

        _countCard.text = _cardData.CountPlayer.ToString();
        currentCountThisCardInDeck = 0;
    }

    /// <summary>
    /// �a���� ������ � ������ ���� � �� ������� ����
    /// </summary>
    private void _reCalculate()
    {

    }

    public void GetCardInDeck()
    {
        int countCard = Convert.ToInt32(_countCard.text);
        
        if (countCard > 0)
        {
            countCard--;
            currentCountThisCardInDeck++;

            _countCardInDeck.text = currentCountThisCardInDeck.ToString();
            _countCard.text = countCard.ToString();
            AddCard?.Invoke(_cardData);
        }
    }
    public void GiveCardInDeck()
    {
        int countCard = Convert.ToInt32(_countCard.text);

        if (countCard < 2)
        {
            countCard++;
            currentCountThisCardInDeck--;

            _countCardInDeck.text = currentCountThisCardInDeck.ToString();
            _countCard.text = countCard.ToString(); 
            RemoveCard?.Invoke(_cardData);
        }
    }
}
