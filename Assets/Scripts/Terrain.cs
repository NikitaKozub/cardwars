using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour
{
    public GameObject ViewTerrain;
    public Field FieldTerrain;
    public Card CardOnTerrain;

    private void OnEnable()
    { 
        ViewTerrain = this.gameObject;
    }
    public IEnumerator Blink()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        meshRenderer.enabled = true;
    }
}
