using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornScepter : Card
{
    public void Action(Card card, int count)
    {
        card.Date.Hp += 1 * count;
    }

    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Number = 
        //Field = Field.;
        Date.TypeCard = TypeCard.Spell;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
