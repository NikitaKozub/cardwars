﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Cards
{ 
    public enum TypeCard
    {
        Creature,
        Structure,
        Earth,
        Spell
    }
}
