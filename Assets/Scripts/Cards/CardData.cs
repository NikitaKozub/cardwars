using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cards
{
    [CreateAssetMenu(fileName = "DateCard", menuName = "Card/CreateNewData", order = 51)]
    public class CardData : ScriptableObject
    {
        public int Number;
        public int Cost;
        public Field Field;
        public string Name;
        public int Attack;
        public int Hp;
        public string Description;
        public TypeCard TypeCard;
        public Image ImageCard;
        public bool IsAttacked = true;
        public bool IsBuy;
        [SerializeField] private int _countPlayer;
        public int CountPlayer
        {
            get
            {
                return _countPlayer;
            }
            set
            {
                if (value > 2 || value < 0)
                {
                    return;
                }
                else
                {
                    _countPlayer = value;
                }
            }
        }
    }
}
