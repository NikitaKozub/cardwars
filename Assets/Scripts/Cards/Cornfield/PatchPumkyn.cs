using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Тыквустер
/// </summary>
public class PatchPumkyn : Card
{
    public void Action(Card card, int count)
    {
        for (int i = 0; i < count; i++)
        {
            card.Date.Hp -= 1;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Date.Field = Field.cornfield;
        //Date.TypeCard = TypeCard.Creature;
        //Date.Hp = 5;
        //Date.Attack = 1;
        //Date.Number = 27;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
