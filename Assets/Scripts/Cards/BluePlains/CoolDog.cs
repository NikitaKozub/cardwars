using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDog : Card
{
    public void Action(Card[] cards)
    {
        foreach(var card in cards)
        {
            card.Date.IsAttacked = false;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Number = 2;
        //Field = Field.blue_plains;
        //TypeCard = TypeCard.��������;
        //Hp = 7;
        //Attack = 2;
        //Cost = 2;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
