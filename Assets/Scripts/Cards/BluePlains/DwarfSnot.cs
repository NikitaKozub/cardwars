using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfSnot : Card
{
    int countCard;
    // Start is called before the first frame update
    void Awake()
    {
        countCard = 3;
    }

    public override void Activate()
    {
        BoardGame boardGame = GameObject.Find("BoardGame").GetComponent<BoardGame>();
        boardGame.GetCardInHand(countCard);
    }

    public override void ToAttack(Card card)
    {
        
    }
}
