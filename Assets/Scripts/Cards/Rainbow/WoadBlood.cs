using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoadBlood : Card
{
    public void Action(Card card, int count)
    {
        card.Date.Attack += 2 * count;
    }

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Field = Field.rainbow_lands;
        //TypeCard = TypeCard.Заклинание;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
