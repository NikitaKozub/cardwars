using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerebralStorm : Card
{
    public void Action(Card[] cards)
    {
        foreach (var card in cards)
        {
            card.Date.Hp -= 1;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Field = Field.rainbow_lands;
        Date.TypeCard = TypeCard.Spell;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
