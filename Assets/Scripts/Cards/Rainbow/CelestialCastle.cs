using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelestialCastle : Card
{
    public void Action(Card card)
    {
        card.Date.Hp += 3;
    }

    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }

    public override void Activate()
    {
        //Field = Field.rainbow_lands;
        //TypeCard = TypeCard.Строение;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
