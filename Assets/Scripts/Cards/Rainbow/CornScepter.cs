using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Cards.Rainbow
{
    public class CornScepter : Card
    {
        public void Action(Card card, int count)
        {
            card.Date.Hp += 1 * count;
        }


        void Awake()
        {
            Activate();
        }

        public override void Activate()
        {
            //CardDate.Field = Field.cornfield;
            //CardDate.TypeCard = TypeCard.Заклинание;
        }

        public override void ToAttack(Card card)
        {
            throw new System.NotImplementedException();
        }
    }
}