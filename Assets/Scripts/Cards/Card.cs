using Assets.Scripts.Cards;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Cards
{
    public abstract class Card : MonoBehaviour
    {
        public float PositionHandX;
        public float PositionHandY;
        public float PositionHandZ;
        public bool IsOwnerTerrain;
        public Terrain Terrain;
        public abstract void ToAttack(Card card);


        public void ToAttack(Player player)
        {
            player.ReceiveDamage(Date.Attack);
        }

        public CardData Date;
        protected void Die()
        {
            Destroy(this.gameObject);
        }

        public void Render()
        {
            TextMesh _cost = this.gameObject.transform.Find("Cost").GetComponent<TextMesh>();
            _cost.text = Date.Cost.ToString();
            TextMesh _hp = this.gameObject.transform.Find("Hp").GetComponent<TextMesh>();
            _hp.text = Date.Hp.ToString();
            TextMesh _attack = this.gameObject.transform.Find("Attack").GetComponent<TextMesh>();
            _attack.text = Date.Attack.ToString();
            TextMesh _typeCard = this.gameObject.transform.Find("TypeCard").GetComponent<TextMesh>();
            _typeCard.text = Date.TypeCard.ToString();
            TextMesh _field = this.gameObject.transform.Find("Field").GetComponent<TextMesh>();
            _field.text = Date.Field.ToString();
            TextMesh _name = this.gameObject.transform.Find("Name").GetComponent<TextMesh>();
            _name.text = Date.Name;
            //TextMesh _description = this.gameObject.transform.Find("Description").GetComponent<TextMesh>();
            //_description.text = Description;
            ss[0] = Resources.Load<Material>(Path.Combine("Image/Card/Materials/" + Date.Number));
            gameObject.transform.Find("�arcass").GetComponent<MeshRenderer>().materials = ss;
        }

        [NonSerialized] public Material[] ss = new Material[1];
        private void OnEnable()
        {
            Render();
        }

        public void MoveInHand(Transform newPosition)
        {
            this.gameObject.transform.position = newPosition.position;
        }

        public abstract void Activate();

        void OnMouseDrag()
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 3); // ���������� ������������� ���������� ���� �� ���� � ������
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition); // ���������� - ������� �������������� ���������� � ������������ ����
            transform.position = objPosition; // � ���������� ������� ������������� ����������
            MoveCardInTerrain?.Invoke(this);
        }    
        void OnMouseUp()
        {
            if (!IsOwnerTerrain)
            {
                this.gameObject.transform.position = new Vector3(PositionHandX, PositionHandY, PositionHandZ);
                IsOwnerTerrain = false;
            }
            else
            {
                CardMouseUp?.Invoke(this);
                this.gameObject.transform.position = Terrain.transform.position;
                this.gameObject.transform.Translate(0, 0, -0.2f);
            }
        }

        public event UnityAction<Card> MoveCardInTerrain;
        public event UnityAction<Card> CardMouseUp;
    }
}
