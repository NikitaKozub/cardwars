﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Cards
{
    public static class StorageCard
    {
        public static Sprite[] FieldsIcons;
        static StorageCard() {
            FieldsIcons = Resources.LoadAll<Sprite>(Path.Combine("Prefabs/UI/FiledsIcons/"));
        }
        
        public static void SaveToFileCards(string filePath, Field field)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }

            CardData[] cardDatas = Resources.LoadAll<CardData>(Path.Combine("Prefabs/PrefabsCards/" + (int)field));
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            string serialized = JsonConvert.SerializeObject(cardDatas, settings);

            File.WriteAllText(filePath, serialized);
        }

        public static bool SaveToFileDeck(string Name, CardData[] cardDatas)
        {
            StringBuilder path = new StringBuilder();
            string __path = Application.dataPath + "/Decks/" + Name;
            path.Append(Path.Combine(Application.dataPath + "/Decks/" + Name+".json"));
            int idDeck = 0;
            for (; ;)
            {
                if (!File.Exists(path.ToString()))
                {
                    File.Create(path.ToString()).Close();
                    break;
                }
                else
                {
                    path.Clear();
                    path.Append(__path + idDeck.ToString() + ".json");
                    idDeck++;
                }
            }

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            string serialized = JsonConvert.SerializeObject(cardDatas, settings);

            File.WriteAllText(path.ToString(), serialized);

            return true;
        }

        public static bool SaveToFileCurrentDeck(CardData[] cardDatas)
        {
            StringBuilder path = new StringBuilder();
            path.Append(Path.Combine(Application.dataPath + "/Decks/CurrentDeck.json"));
            if (!File.Exists(path.ToString()))
            {
                File.Create(path.ToString()).Close();
            }
            else
            {
                File.Delete(path.ToString());
                File.Create(path.ToString()).Close();
            }

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            string serialized = JsonConvert.SerializeObject(cardDatas, settings);

            File.WriteAllText(path.ToString(), serialized);

            return true;
        }

        public static CardData[] LoadDataFromFile(string path)
        {
            var json = File.ReadAllText(path);
            CardData[] cardDatas = JsonConvert.DeserializeObject<CardData[]>(json);
            return cardDatas;
        }

        public static CardData[] LoadDataFromFile(Field field)
        {
            var json = File.ReadAllText(Path.Combine(Application.dataPath + "/" + "CardsCollection" + field + ".json"));
            CardData[] cardDatas = JsonConvert.DeserializeObject<CardData[]>(json);
            return cardDatas;
        }

        public static CardData[] LoadDeck()
        {
            StringBuilder path = new StringBuilder();
            path.Append(Path.Combine(Application.dataPath + "/Decks/CurrentDeck.json"));

            var json = File.ReadAllText(path.ToString());
            CardData[] cardDatas = JsonConvert.DeserializeObject<CardData[]>(json);
            return cardDatas;
        }
    }
}
