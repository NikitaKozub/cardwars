using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFrostCard : Card 
{
    // Start is called before the first frame update
    void Awake()
    {
        Activate();
    }


    public override void Activate()
    {
        //Field = Field.frosty_lands;
        Date.TypeCard = TypeCard.Creature;
        Date.Hp = 7;
        Date.Attack = 1;
    }

    public override void ToAttack(Card card)
    {
        throw new System.NotImplementedException();
    }
}
