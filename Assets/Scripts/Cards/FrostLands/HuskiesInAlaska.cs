﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Cards
{
    public class HuskiesInAlaskaCard : Card
    {

        // Start is called before the first frame update
        void Awake()
        {
            Activate();
        } 

        public override void Activate()
        {
            Date.Attack = 1;
            Date.Hp = 7;
            //Field = Field.frosty_lands;
            Date.Cost = 2;
        }
        public override void ToAttack(Card card)
        {
            throw new NotImplementedException();
        }
    }
}
