﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Cards
{
    public class Deck
    {
        public string Name;
        public Field Field;
        public CardData[] Cards;
        public Deck(CardData[] cards)
        {
            Cards = cards;
        }
    }
}
