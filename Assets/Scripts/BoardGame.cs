using Assets.Scripts.Cards;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
public class BoardGame : MonoBehaviour
{
    private ManagerFight _managerFight;
    public Card[] cardsPlayer1;
    public Card[] cardsPlayer2;
    [SerializeField] GameObject TemplateCard;
    private Player _player;
    private Player _playerEnemy;

    private void Awake()
    {
        _managerFight = GameObject.Find("Main Camera").GetComponent<ManagerFight>();
        _player = GameObject.Find("Player").GetComponent<Player>();
        _player.CurrentDataDeck = StorageCard.LoadDeck();

        _playerEnemy = GameObject.Find("PlayerEnemy").GetComponent<Player>();
        _playerEnemy.CurrentDataDeck = StorageCard.LoadDeck();

        HashSet<Field> fields = new HashSet<Field>();
        List<Card[]> cards = new List<Card[]>();
        for (int i = 0; i < _player.CurrentDataDeck.Length; i++)
        {
            fields.Add(_player.CurrentDataDeck[i].Field);
        }

        foreach (var itr in fields)
        {
            cards.Add(Resources.LoadAll<Card>("Prefabs/PrefabsCards/" + (int)itr + "/"));
        }

        _player.CreateCardForGame(cards);
        _playerEnemy.CreateCardForGame(cards);

        cardsPlayer1 = CardMixer.MixerCards(cardsPlayer1);
        cardsPlayer2 = CardMixer.MixerCards(cardsPlayer2);

        StartGame();
    }

    public void GetCardInHand(int count)
    {
        if (_player.CurrentDeck.Count > 0)
        {
            _player.HandPlayer.AddCardInHand(_player.CurrentDeck.First());
            _player.CurrentDeck.Remove(_player.CurrentDeck.First());
        }
        else
        {

        }

        _playerEnemy.TerrainPlayer[0].CardOnTerrain =  _playerEnemy.CurrentDeck.First();
    }

    public void StartGame()
    {
        for (int i = 0; i < 5; i++)
        {
            _player.HandPlayer.AddCardInHand(_player.CurrentDeck.First());
            _player.CurrentDeck.Remove(_player.CurrentDeck.First());
        }
    }

    private void OnEnable()
    {
        _managerFight.StartTurnGame += GetCardInHand;
        _managerFight.EndTurnGame += FigthCreatureInEndTurn;
    }

    private void OnDisable()
    {
        _managerFight.StartTurnGame -= GetCardInHand;
        _managerFight.EndTurnGame -= FigthCreatureInEndTurn;
    }

    public void FigthCreatureInEndTurn()
    {
        for (int i = 0; i < _player.TerrainPlayer.Length; i++)
        {
            if (_player.TerrainPlayer[i].CardOnTerrain != null)
            {
                if (_playerEnemy.TerrainPlayer[i].CardOnTerrain == null)
                {
                    _player.TerrainPlayer[i].CardOnTerrain.ToAttack(_playerEnemy);
                }
                else
                {
                    _player.TerrainPlayer[i].CardOnTerrain.ToAttack(_playerEnemy.TerrainPlayer[i].CardOnTerrain);
                }
            }
            else
            {
                if (_player.TerrainPlayer[i].CardOnTerrain == null)
                {
                    continue;
                }
                else
                {
                    _player.TerrainPlayer[i].CardOnTerrain.ToAttack(_playerEnemy);
                }
            }
        }
    }
}
